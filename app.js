let balances = initializeBalances()
while (balances.human > 0 && balances.bot > 0) {
   let winners = playGame(balances, process.stdin, process.stdout);
      for (const winner of winners) {
         balances[winner.name] += winner.gain
      }
}

displayWinners(balances);